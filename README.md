**Running Jenkins as Docker Container**

1. after pull this repo set the volume place as read able 
```bash
chmod -R 777 jenkins_data
```
2. runing the compose file
```bash
docker-compose up -d
```
3. after that runing 
```bash
docker ps
```
4. to get default password

```bash
docker container exec \
[CONTAINER ID or NAME] \
    sh -c "cat /var/jenkins_home/secrets/initialAdminPassword"
```
```bash
docker container exec f8a86daae71e sh -c "cat /var/jenkins_home/secrets/initialAdminPassword"
```
the script will return the default password of jenkins
